# -*- coding: utf-8 -*-
# Copyright (C) 2014 Edammani,Fick,Hennecke IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import EatOnEvent

class EatOnAction(Action2):
    EVENT = EatOnEvent
    ACTION = "eat-on"
    RESOLVE_OBJECT = "resolve_for_use"
