# -*- coding: utf-8 -*-
# Copyright (C) 2014 Edammani,Fick,Hennecke IUT d'Orléans
#==============================================================================

from .event import Event2

class EatOnEvent(Event2):
    NAME = "Eat-on"

    def perform(self):
        if not self.object.has_prop("eatable"):
            self.fail()
            return self.inform("eat-on.failed")
        self.inform("eat-on")



